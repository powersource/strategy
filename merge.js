module.exports = {
  Merge (composition) {
    // composition is an Object made up of several strategy

    return function merge (graph, mergeNode) {
      const c = {}
      Object.entries(composition).forEach(([field, strategy]) => {
        c[field] = strategy.merge(graph, mergeNode, field)
      })
      return c
    }
  },

  IsConflict (composition) {
    return function isConflict (graph, tipIds) {
      // graph = a graph containing nodeIds, transforms, previous, and history
      // tipIds = the tips of branches in the graph that may be conflicting
      return Object.entries(composition).some(([field, strategy]) => {
        return strategy.isConflict(graph, tipIds, field)
      })
    }
  },

  IsValidMerge (composition) {
    return function isValidMerge (graph, mergeNode) {
      isValidMerge.error = null
      isValidMerge.errors = []
      isValidMerge.fields = []

      // graph = a graph containing nodeIds, transforms, previous, and history
      // tipId = the merge node
      const errors = []
      Object.entries(composition).forEach(([field, strategy]) => {
        const result = strategy.isValidMerge(graph, mergeNode, field)
        if (!result) {
          errors.push({
            field,
            detail: strategy.isValidMerge.error || new Error(`merge error on ${field}`)
          })
        }
      })
      if (errors.length > 0) {
        const fields = errors.map(e => e.field)
        isValidMerge.error = new Error(`Merge conflict on fields [${fields}]`)
        isValidMerge.errors = errors.map(e => e.detail)
        isValidMerge.fields = fields
      }
      return errors.length === 0
    }
  }
}
