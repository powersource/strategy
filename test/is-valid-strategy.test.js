const test = require('tape')
const Strategy = require('../')
const { Overwrite, SimpleSet } = require('./strategies')

test('Strategy.isValid + isValidComposition', t => {
  t.equal(Strategy.isValid(Overwrite()), true, '@tangle/overwrite isValid')
  t.equal(Strategy.isValid(SimpleSet()), true, '@tangle/simple-set isValid')

  const badStrategy = {
    identity: () => 'dog',
    mapToOutput: () => 'boop',
    concat: (a, b) => a + b, // doesn't identity doesn't work here!
    isValid: () => true
    /* missing */

    // schema
    // isConflict
    // isValidMerge
    // merge
  }
  t.equal(Strategy.isValid(badStrategy), false, 'isValid spots invalid strategy')
  console.log(Strategy.isValid.error.message)
  t.match(Strategy.isValid.error.message, /Invalid strategy/, 'nice error message')

  const compositon = {
    title: Overwrite(),
    attendees: SimpleSet()
  }
  t.equal(Strategy.isValidComposition(compositon), true, 'isValidComposition')

  const badComposition = {
    title: Overwrite(),
    attendees: badStrategy
  }
  t.equal(Strategy.isValidComposition(badComposition), false, 'isValidComposition spot bad composition')
  t.match(Strategy.isValidComposition.error.message, /Invalid composition/)

  const strategy = new Strategy(compositon)
  t.equal(Strategy.isValid(strategy), true, 'composed Strategy isValid')
  t.end()
})
