module.exports = function Schema (composition) {
  const schema = {
    type: 'object',
    properties: {},
    additionalProperties: false
  }

  for (const field in composition) {
    schema.properties[field] = composition[field].schema
  }

  return schema
}
