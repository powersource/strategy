const isEqual = require('lodash.isequal')

function isValidStrategy (strategy, label) {
  isValidStrategy.error = null
  const errors = []

  if (typeof strategy.schema !== 'object') errors.push('must have a schema')
  if (typeof strategy.isValid !== 'function') errors.push('must have an isValid method')
  if (typeof strategy.concat !== 'function') errors.push('must have a concat method')
  if (typeof strategy.isConflict !== 'function') errors.push('must have a isConflict method')
  if (typeof strategy.isValidMerge !== 'function') errors.push('must have a isValidMerge method')
  if (typeof strategy.merge !== 'function') errors.push('must have a merge method')

  if (strategy.mapFromInput && typeof strategy.mapFromInput !== 'function') {
    errors.push('if present mapFromInput must be a function')
  }
  if (typeof strategy.mapToOutput !== 'function') errors.push('must have a mapToOutput method')

  if (typeof strategy.identity !== 'function') errors.push('must have an identity method')
  else if (strategy.mapToOutput && strategy.concat) {
    const i = strategy.identity()
    if (i === undefined) errors.push('cannot have an identity element equal to "undefined"')
    // undefined not supported by JSON
    else if (strategy.mapToOutput(i) === undefined) errors.push('mapToOutput(identity()) must not equal undefined')
    else if (!isEqual(strategy.concat(i, i), i)) errors.push('concat(identity, identity) !== identity')
  }

  if (errors.length === 0) return true

  isValidStrategy.error = new Error([
    'Invalid strategy:',
    ...errors.map(reason => error(label, reason))
  ].join('\n - '))
  return false
}

function error (label, reason) {
  if (!label) return `strategy ${reason}`

  return `strategy for ${label} ${reason}`
}

module.exports = isValidStrategy
